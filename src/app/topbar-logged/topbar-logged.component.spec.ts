import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopbarLoggedComponent } from './topbar-logged.component';

describe('TopbarLoggedComponent', () => {
  let component: TopbarLoggedComponent;
  let fixture: ComponentFixture<TopbarLoggedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopbarLoggedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TopbarLoggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
