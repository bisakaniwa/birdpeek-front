import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';

import { SampleComponent } from './sample.component';

describe('SampleComponent', () => {
  let component: SampleComponent;
  let fixture: ComponentFixture<SampleComponent>;
  const birds: Bird[] = [{name: 'Rolinha', image: 'URL'}];

  class BirdServiceMock extends BirdService{
    
    override getAll(): Observable<Bird[]> {      
      return of(birds);
    }
  }
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [ SampleComponent ],
      providers: [{provide: BirdService, useClass: BirdServiceMock}]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => { 
    expect(component).toBeTruthy();
  });

  it('should get all birds', ()=>{
    component.getAll();
    expect(component.birds.length).toBeGreaterThan(0);
  })
});
