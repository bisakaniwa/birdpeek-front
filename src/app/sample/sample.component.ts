import { Component, OnInit } from '@angular/core';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.css']
})
export class SampleComponent implements OnInit {

  birds: Bird[] = []
  bird: Bird = {} as Bird

  constructor(
    private service: BirdService,
  ) { }

  ngOnInit(): void {
    this.getAll();
  }


  getAll(): void {
    this.service.getAll().subscribe(allBirds => {
      this.birds = allBirds;
    })
  }
}
