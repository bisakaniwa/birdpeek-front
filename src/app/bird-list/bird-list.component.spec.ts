import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BindingType } from '@angular/compiler';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';

import { BirdListComponent } from './bird-list.component';

describe('BirdListComponent', () => {
  let component: BirdListComponent;
  let fixture: ComponentFixture<BirdListComponent>;
  let birdService;

  const birds: Bird[] = [{name: 'Rolinha', image: 'URL'}]

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ BirdListComponent ]
    })
    .compileComponents();
    birdService = TestBed.inject(BirdService);
    spyOn(birdService, 'getAll').and.returnValue(of(birds));
    fixture = TestBed.createComponent(BirdListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get all birds', ()=>{
    component.getAll();
    expect(component.birds.length).toBeGreaterThan(0);
    expect(component.birds).toBe(birds);
  })
});
