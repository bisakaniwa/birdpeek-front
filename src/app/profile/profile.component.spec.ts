import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { User } from '../interface/User.interface';
import { UserService } from '../service/user.service';

import { ProfileComponent } from './profile.component';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  let userService;
  const user: User = {id: 1, firstName: 'Bianca', username: 'bisakaniwa', email: 'adm@birdpeek.com', password: 'birdpeeker'}


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ ProfileComponent ]
    })
    .compileComponents();
    userService = TestBed.inject(UserService);
    spyOn(userService, 'getById').and.returnValue(of(user))
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should find an user by ID', () => {
    component.findUser(1);
    expect(component.user.id).toEqual(user.id);
  });
});
