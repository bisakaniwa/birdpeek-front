import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Type } from '@angular/core';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { Observation } from '../interface/Observation.interface'
import { ObservationService } from './observation.service';

describe('ObservationService', () => {
  let service: ObservationService;
  let httpMock: HttpTestingController;

  const observations: Observation[] = [{idObs: 1, birdObs: 'Rolinha', dateObs: '01/11/2022', timeObs: '14:00', locationObs: 'right here'}]
  const observation: Observation = {idObs: 1, birdObs: 'Rolinha', dateObs: '01/11/2022', timeObs: '14:00', locationObs: 'right here'}

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
    });
    service = TestBed.inject(ObservationService);
    httpMock = TestBed.inject(HttpTestingController as Type<HttpTestingController>);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all observations', fakeAsync(() => {
    service.getAll().subscribe(response => {
      expect(response).toEqual(observations);
    })
    const req = httpMock.expectOne(service.observationsUrl);
    expect(req.request.method).toBe('GET')
  }))

  it('should get observation by ID', fakeAsync(() => {
    service.getById(1).subscribe(response => {
      expect(response).toEqual(observation);
    })
    const req = httpMock.expectOne(service.observationsUrl + '/1');
    expect(req.request.method).toBe('GET')
  }))

  it('should save a new bird observation', fakeAsync(() => {
    service.save(observation).subscribe(response => {
      expect(response).toEqual(observation);
    })
    const req = httpMock.expectOne(service.observationsUrl);
    expect(req.request.method).toBe('POST')
  }))

  it('should update a bird observation', fakeAsync(() => {
    service.update(observation).subscribe(response => {
      expect(response).toEqual(observation);
    })
    const req = httpMock.expectOne(service.observationsUrl + '/update');
    expect(req.request.method).toBe('PUT')
  }))

  it('should delete an observation by ID', fakeAsync(() => {
    service.deleteById(1).subscribe(response => {
      expect(response).toEqual(observation);
    })
    const req = httpMock.expectOne(service.observationsUrl + '/1');
    expect(req.request.method).toBe('DELETE')
  }))
});
