import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../interface/User.interface'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public usersUrl: string = 'http://localhost:8080/user'

  constructor(private http: HttpClient) { }

  getById(id: number) : Observable<User> {
    return this.http.get<User>("http://localhost:8080/user/" + id)
  }

  saveUser(user: User) : Observable<User> {
    return this.http.post<User>("http://localhost:8080/user", user)
  }
}
