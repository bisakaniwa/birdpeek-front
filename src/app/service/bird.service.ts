import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bird } from '../interface/Bird.interface';

@Injectable({
  providedIn: 'root'
})
export class BirdService { 

  public birdsUrl: string = 'http://localhost:8080/bird';

  constructor(private httpClient: HttpClient) { } 

  getAll(): Observable<Bird[]> {
    return this.httpClient.get<Bird[]>(this.birdsUrl);
  }

  getById(id: number): Observable<Bird> {
    return this.httpClient.get<Bird>(`${this.birdsUrl}/${id}`);
  }

  getByNamePtEn(search: string) : Observable<Bird[]> {
    return this.httpClient.get<Bird[]>(`${this.birdsUrl}/search/${search}`);
  }

  save(bird: Bird): Observable<Bird> {
    return this.httpClient.post<Bird>(`${this.birdsUrl}`, bird);
  }

  update(bird: Bird): Observable<Bird> {
    return this.httpClient.put<Bird>(`${this.birdsUrl}/update`, bird);
  }

  deleteById(id: number): Observable<Bird> {
    return this.httpClient.delete<Bird>(`${this.birdsUrl}/${id}`);
  }

}
