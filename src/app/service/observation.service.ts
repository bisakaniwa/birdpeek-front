import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Observation } from '../interface/Observation.interface';

@Injectable({
  providedIn: 'root'
})
export class ObservationService { 

  public observationsUrl: string = 'http://localhost:8080/observation';

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Observation[]> {
    return this.httpClient.get<Observation[]>(this.observationsUrl);
  }

  getById(idObs: number): Observable<Observation> {
    return this.httpClient.get<Observation>(`${this.observationsUrl}/${idObs}`);
  }

  save(observation: Observation): Observable<Observation> {
    return this.httpClient.post<Observation>(`${this.observationsUrl}`, observation);
  }

  update(observation: Observation): Observable<Observation> {
    return this.httpClient.put<Observation>(`${this.observationsUrl}/update`, observation);
  }

  deleteById(idObs: number): Observable<Observation> {
    return this.httpClient.delete<Observation>(`${this.observationsUrl}/${idObs}`);
  }

}
