import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { User } from '../interface/User.interface';
import { Type } from '@angular/core';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;
  let httpMock: HttpTestingController

  const user: User = {id: 1, firstName: 'Bianca', username: 'bisakaniwa', email: 'adm@birdpeek.com', password: 'birdpeeker'}

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
    });
    service = TestBed.inject(UserService);
    httpMock = TestBed.inject(HttpTestingController as Type<HttpTestingController>);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get an user by ID', fakeAsync(() => {
    service.getById(1).subscribe(response => {
      expect(response).toEqual(user);
    })
    const req = httpMock.expectOne(service.usersUrl + '/1');
    expect(req.request.method).toBe('GET')
  }))

  it('should save a new user', fakeAsync(() => {
    service.saveUser(user).subscribe(response => {
      expect(response).toEqual(user);
    })
    const req = httpMock.expectOne(service.usersUrl);
    expect(req.request.method).toBe('POST')
  }))
});
