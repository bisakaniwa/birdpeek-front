import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { Type } from '@angular/core';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { Bird } from '../interface/Bird.interface'
import { BirdService } from './bird.service';

describe('BirdService', () => {
  let service: BirdService;
  let httpMock: HttpTestingController;

  const birds: Bird[] = [{name: 'Rolinha', image: 'URL'}];
  const bird: Bird = {id: 13, name: 'Rolinha', image: 'URL'};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        BirdService
      ],
    });
    service = TestBed.inject(BirdService);
    httpMock = TestBed.inject(HttpTestingController as Type<HttpTestingController>);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  }); 

  it('should get a list of all birds', fakeAsync(() => {    
    service.getAll().subscribe(response => {     
      expect(response).toEqual(birds);         
    })
    const req = httpMock.expectOne(service.birdsUrl);
    expect(req.request.method).toBe('GET')
  }))

  it('should find a bird by ID', fakeAsync(() => {
    service.getById(1).subscribe(response => {
      expect(response).toEqual(bird);
    })
    const req = httpMock.expectOne(service.birdsUrl+ '/1');
    expect(req.request.method).toBe('GET')
  }))

  it('should find a bird by portuguese or english name', fakeAsync(() => {
    service.getByNamePtEn('Rolinha').subscribe(response => {
      expect(response).toEqual(birds);
    })
    const req = httpMock.expectOne(service.birdsUrl +'/search/Rolinha');
    expect(req.request.method).toBe('GET')
  }))

  it('should save a new bird', fakeAsync(() => {
    service.save(bird).subscribe(response => {
      expect(response).toEqual(bird);
    })
    const req = httpMock.expectOne(service.birdsUrl);
    expect(req.request.method).toBe('POST')
  }))

  it('should update a bird data', fakeAsync(() => {
    service.update(bird).subscribe(response => {
      expect(response).toEqual(bird);
    })
    const req = httpMock.expectOne(service.birdsUrl + '/update');
    expect(req.request.method).toBe('PUT')
  }))

  it('should delete a bird by ID', fakeAsync(() => {
    service.deleteById(1).subscribe(response => {
      expect(response).toEqual(bird);
    })
    const req = httpMock.expectOne(service.birdsUrl + '/1');
    expect(req.request.method).toBe('DELETE')
  }))
});
