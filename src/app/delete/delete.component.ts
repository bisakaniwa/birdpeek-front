import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';
import { ModalService } from '../_modal';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  bird: Bird = {} as Bird

  @Input()
  id: number = 0;
  
  constructor(
    private service: BirdService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private modalService: ModalService
  ) { }

  ngOnInit(): void {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.findBird(id);
  }

  findBird(id: number) {
    this.service.getById(id).subscribe(result => {
      this.bird = result
    })
  }

  deleteById(id: number) {
    this.service.deleteById(id).subscribe(deleted => {
      this.bird = deleted;
      this.router.navigate(['/bird-list'])
    })
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

}
