import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-topbar-not-logged',
  templateUrl: './topbar-not-logged.component.html',
  styleUrls: ['./topbar-not-logged.component.css']
})
export class TopbarNotLoggedComponent implements OnInit {

  constructor(
  ) { }

  ngOnInit(): void {
  }
}
