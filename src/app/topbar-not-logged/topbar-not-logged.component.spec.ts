import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopbarNotLoggedComponent } from './topbar-not-logged.component';

describe('TopbarNotLoggedComponent', () => {
  let component: TopbarNotLoggedComponent;
  let fixture: ComponentFixture<TopbarNotLoggedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopbarNotLoggedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TopbarNotLoggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
