import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { SearchComponent } from './search.component';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  // let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchComponent ],
      // providers: [{provide: Router, useValue: router }]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect to result component', inject([Router],  ( router: Router)=>{
    spyOn(router, 'navigate').and.stub();
    component.searching();
    expect(router.navigate).toHaveBeenCalledWith(['/result', ''])
  }))
});
