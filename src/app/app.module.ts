import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { AddComponent } from './add/add.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { UpdateComponent } from './update/update.component';
import { BirdListComponent } from './bird-list/bird-list.component';
import { SingleBirdComponent } from './single-bird/single-bird.component';
import { ModalModule } from './_modal';
import { DeleteComponent } from './delete/delete.component';
import { SearchComponent } from './search/search.component';
import { ResultComponent } from './result/result.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { TopbarNotLoggedComponent } from './topbar-not-logged/topbar-not-logged.component';
import { TopbarLoggedComponent } from './topbar-logged/topbar-logged.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './footer/footer.component';
import { MyObservationsComponent } from './my-observations/my-observations.component';
import { AddObservationComponent } from './add-observation/add-observation.component';
import { UpdateObservationComponent } from './update-observation/update-observation.component';
import { ProfileComponent } from './profile/profile.component';
import { SampleComponent } from './sample/sample.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddComponent,
    UpdateComponent,
    BirdListComponent,
    SingleBirdComponent,
    DeleteComponent,
    SearchComponent,
    ResultComponent,
    SignUpComponent,
    UserHomeComponent,
    TopbarNotLoggedComponent,
    TopbarLoggedComponent,
    LoginComponent,
    FooterComponent,
    MyObservationsComponent,
    AddObservationComponent,
    UpdateObservationComponent,
    ProfileComponent,
    SampleComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
