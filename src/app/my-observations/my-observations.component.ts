import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observation } from '../interface/Observation.interface';
import { ObservationService } from '../service/observation.service';
import { ModalService } from '../_modal/modal.service';

@Component({
  selector: 'app-my-observations',
  templateUrl: './my-observations.component.html',
  styleUrls: ['./my-observations.component.css']
})
export class MyObservationsComponent implements OnInit {

  observations: Observation[] = []
  observation: Observation = {} as Observation
  idObs: number = 0;

  constructor(
    private service: ObservationService,
    private router: Router,
    private modalService: ModalService
    ) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(): void {
    this.service.getAll().subscribe(allObservations => {
      this.observations = allObservations;
    })
  }

  updateObservation(idObs: number) {
    this.router.navigate(['/update-observation', idObs])
  }

  deleteById() {
    this.service.deleteById(this.idObs).subscribe({      
       complete: () => location.reload()
    })   
  }

  openModal(id: string, idObs: number) {
    this.idObs = idObs;
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }
}
