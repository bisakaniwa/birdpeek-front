import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observation } from '../interface/Observation.interface';

import { MyObservationsComponent } from './my-observations.component';

describe('MyObservationsComponent', () => {
  let component: MyObservationsComponent;
  let fixture: ComponentFixture<MyObservationsComponent>;

  const observation: Observation = {idObs: 1, birdObs: 'Rolinha', dateObs: '01/11/2022', timeObs: '14:00', locationObs: 'right here'}

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ MyObservationsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyObservationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect to updateObservation component', inject([Router], (router: Router) => {
    spyOn(router, 'navigate').and.stub();
    component.updateObservation(1);
    expect(router.navigate).toHaveBeenCalledWith(['/update-observation', 1])
  }));
});
