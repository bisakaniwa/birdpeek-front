import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Bird } from '../interface/Bird.interface';
import { Observation } from '../interface/Observation.interface';
import { BirdService } from '../service/bird.service';
import { ObservationService } from '../service/observation.service';

@Component({
  selector: 'app-add-observation',
  templateUrl: './add-observation.component.html',
  styleUrls: ['./add-observation.component.css']
})
export class AddObservationComponent implements OnInit {

  observation: Observation = {} as Observation
  bird: Bird = {} as Bird
  birds: Bird[] = []

  constructor(
    private service: ObservationService,
    private birdService: BirdService,
    private form: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getAll();
  }

  addForm = this.form.group({
    birdObs: ['', Validators.required],
    dateObs: ['', Validators.required],
    timeObs: ['', Validators.required],
    locationObs: ['', Validators.required]
  });

  getAll(): void {
    this.birdService.getAll().subscribe(allBirds => {
      this.birds = allBirds;
    })
  }

  adding(observation: Observation) {
    if (this.addForm.value.birdObs !== '' && this.addForm.value.dateObs !== '' && this.addForm.value.timeObs !== '' && this.addForm.value.locationObs !== '') {
      this.observation.birdObs = this.addForm.value.birdObs!;
      this.observation.dateObs = this.addForm.value.dateObs!;
      this.observation.timeObs = this.addForm.value.timeObs!;
      this.observation.locationObs = this.addForm.value.locationObs!;

      this.service.save(observation).subscribe({
        next: (newObservation: Observation) => this.observation = newObservation,
        complete: () => {
          alert("You just peek a bird!");
          this.router.navigate(['/my-observations']);
        }
      });
    }
  }
}
