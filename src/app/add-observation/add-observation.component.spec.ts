import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, inject, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { Bird } from '../interface/Bird.interface';
import { Observation } from '../interface/Observation.interface';
import { BirdService } from '../service/bird.service';
import { ObservationService } from '../service/observation.service';

import { AddObservationComponent } from './add-observation.component';

describe('AddObservationComponent', () => {
  let component: AddObservationComponent;
  let fixture: ComponentFixture<AddObservationComponent>;
  let birdService;

  const birds: Bird[] = [{name: 'Rolinha', image: 'URL'}]
  const observation: Observation = {
    idObs: 1,
    birdObs: 'Rolinha',
    dateObs: '01/11/2022',
    timeObs: '14:00',
    locationObs: 'right here'
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      declarations: [ AddObservationComponent ]
    })
    .compileComponents();
    birdService = TestBed.inject(BirdService);
    spyOn(birdService, 'getAll').and.returnValue(of(birds))
    fixture = TestBed.createComponent(AddObservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get a list of all birds', () => {    
    component.getAll();
    expect(component.birds).toBe(birds);
  }); 

  it('should save a new observation', () => {
    const event = spyOn(component, "adding");
    component.addForm.value.birdObs = 'Rolinha';
    component.adding(observation);
    expect(component.addForm.value.birdObs).toContain(observation.birdObs);
    expect(event).toHaveBeenCalled();
  });
});
