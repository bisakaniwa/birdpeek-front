import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';

import { SingleBirdComponent } from './single-bird.component';

describe('SingleBirdComponent', () => {
  let component: SingleBirdComponent;
  let fixture: ComponentFixture<SingleBirdComponent>;
  let birdService;
  const bird: Bird = {id: 1, name: 'Rolinha', image: 'URL'}


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ SingleBirdComponent ]
    })
    .compileComponents();
    birdService = TestBed.inject(BirdService);
    spyOn(birdService, 'getById').and.returnValue(of(bird));
    fixture = TestBed.createComponent(SingleBirdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should find a bird by ID', () => {
    component.findBird(1);
    expect(component.bird).toEqual(bird);
  });

  it('should redirect to update component', inject([Router], (router: Router) => {
    spyOn(router, 'navigate').and.stub();
    component.updateBird(1);
    expect(router.navigate).toHaveBeenCalledWith(['/update', 1]);
  }));

  it('should redirect to delete component', inject([Router], (router: Router) => {
    spyOn(router, 'navigate').and.stub();
    component.deleteBird(1);
    expect(router.navigate).toHaveBeenCalledWith(['/delete', 1]);
  }));
});
