import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';

@Component({
  selector: 'app-single-bird',
  templateUrl: './single-bird.component.html',
  styleUrls: ['./single-bird.component.css']
})
export class SingleBirdComponent implements OnInit {

  bird: Bird = {} as Bird
  
  constructor(
    private service: BirdService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.findBird(id);
  }

  findBird(id: number) {
    this.service.getById(id).subscribe(result => {
      this.bird = result
    })
  }

  updateBird(id: number) {
    this.router.navigate(['/update', id])
  }

  deleteBird(id: number) {
    this.router.navigate(['/delete', id])
  }

}
