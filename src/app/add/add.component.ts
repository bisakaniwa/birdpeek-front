import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  bird: Bird = {} as Bird

  constructor(
    private service: BirdService,
    private form: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  addForm = this.form.group({
    name: ['', Validators.required],
    scientific: '',
    englishName: '',
    image: ['', Validators.required],
    activity: '',
    eatingHabits: '',
    migration: '',
  });

  adding(bird: Bird) {
    if(this.addForm.value.name !== '' && this.addForm.value.image !== '') {
      this.bird.name = this.addForm.value.name!;
      this.bird.scientificName = this.addForm.value.scientific!;
      this.bird.englishName = this.addForm.value.englishName!;
      this.bird.image = this.addForm.value.image!;
      this.bird.activity = this.addForm.value.activity!;
      this.bird.eatingHabits = this.addForm.value.eatingHabits!;
      this.bird.migration = this.addForm.value.migration!;      
      
      this.service.save(bird).subscribe(newBird => {
        this.bird = newBird;
        this.router.navigate(['/bird-list']);
      });

      alert("New bird flying around!");

    }
  
  }

}
