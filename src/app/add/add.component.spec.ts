import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';

import { AddComponent } from './add.component';

describe('AddComponent', () => {
  let component: AddComponent;
  let fixture: ComponentFixture<AddComponent>;

  const bird: Bird = {
    name: 'Rolinha', 
    scientificName: '', 
    englishName: '', 
    image: 'URL', 
    activity: '', 
    eatingHabits: '', 
    migration: '' };

  class BirdServiceMock extends BirdService{
    
    override save(bird: Bird): Observable<Bird> {
      return of(bird);
    }
  }
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      declarations: [ AddComponent ],
      providers: [{provider: BirdService, useClass: BirdServiceMock}]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => { 
    expect(component).toBeTruthy();
  });

  it('should save a bird', inject([Router], (router: Router)=>{   
    const evento = spyOn(component, "adding");
    component.addForm.value.name = 'Rolinha';
    component.adding(bird);    
    expect(component.addForm.value.name).toContain(bird.name);
    expect(evento).toHaveBeenCalled();
  }))
});
