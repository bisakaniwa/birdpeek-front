import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddObservationComponent } from './add-observation/add-observation.component';
import { AddComponent } from './add/add.component';
import { BirdListComponent } from './bird-list/bird-list.component';
import { DeleteComponent } from './delete/delete.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MyObservationsComponent } from './my-observations/my-observations.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ProfileComponent } from './profile/profile.component';
import { ResultComponent } from './result/result.component';
import { SampleComponent } from './sample/sample.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SingleBirdComponent } from './single-bird/single-bird.component';
import { UpdateObservationComponent } from './update-observation/update-observation.component';
import { UpdateComponent } from './update/update.component';
import { UserHomeComponent } from './user-home/user-home.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'bird-list',
    component: BirdListComponent
  },
  {
    path: 'single-bird/:id',
    component: SingleBirdComponent
  },
  { 
    path: 'add',
    component: AddComponent
  },
  {
    path: 'update/:id',
    component: UpdateComponent
  },
  {
    path: 'delete',
    component: DeleteComponent
  },
  {
    path: 'result/:search',
    component: ResultComponent
  },
  {
    path: 'result/single-bird/:id', 
    redirectTo: 'single-bird/:id', 
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'sign-up', 
    component: SignUpComponent 
  },
  { 
    path: 'user', 
    component: UserHomeComponent 
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'my-observations',
    component: MyObservationsComponent
  },
  {
    path: 'add-observation',
    component: AddObservationComponent
  },
  {
    path: 'update-observation/:idObs',
    component: UpdateObservationComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'sample',
    component: SampleComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
