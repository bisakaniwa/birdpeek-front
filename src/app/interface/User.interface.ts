export interface User {
    id?: number;
    firstName: string;
    lastName?: string;
    username: string;
    email: string;
    password: string;
    picture?: string;
    birthDate?: string;
}