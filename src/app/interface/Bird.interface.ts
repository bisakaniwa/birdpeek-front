export interface Bird {
    id?: number;
    name: string;
    scientificName?: string;
    englishName?: string;
    image: string;
    activity?: string;
    eatingHabits?: string;
    migration?: string;
}