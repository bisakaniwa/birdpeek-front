export interface Observation {
    idObs?: number;
    birdObs: string;
    dateObs: string;
    timeObs: string;
    locationObs: string;
}