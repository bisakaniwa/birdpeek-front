import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { User } from '../interface/User.interface';
import { UserService } from '../service/user.service';

import { SignUpComponent } from './sign-up.component';

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;

  const user: User = {id: 1, firstName: 'Bianca', username: 'bisakaniwa', email: 'adm@birdpeek.com', password: 'birdpeeker'}

  class UserServiceMock extends UserService {

    override saveUser(user: User): Observable<User> {
      return of(user)
    }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      declarations: [ SignUpComponent ],
      providers: [
        { provider: UserService, useClass: UserServiceMock }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save a new user', inject([Router], (router: Router) => {
    const event = spyOn(component, 'register');
    component.registerForm.value.firstName = 'Bianca';
    component.register(user);
    expect(component.registerForm.value.firstName).toContain(user.firstName);
    expect(event).toHaveBeenCalled();
  }))
});
