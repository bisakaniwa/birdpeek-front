import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { User }from '../interface/User.interface'
import { FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from './must-match.validator';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  user: User = {} as User

  constructor(
    private service: UserService,
    private router: Router,
    private form: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  registerForm = this.form.group ({
    firstName: ['', Validators.required],
    lastName: '',
    username: ['', Validators.required],
    email: ['', Validators.required, Validators.email],
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required],
    picture: '',
    birthDate: ''
  },
  { validator: MustMatch('password', 'confirmPassword')}
  )

  register(user: User) {
    if (this.registerForm.value.email !== '' && this.registerForm.value.password === this.registerForm.value.confirmPassword) {
      this.user.firstName = this.registerForm.value.firstName!;
      this.user.lastName = this.registerForm.value.lastName ? this.registerForm.value.lastName : '';
      this.user.username = this.registerForm.value.username!;
      this.user.email = this.registerForm.value.email!;
      this.user.password = this.registerForm.value.password!;
      this.user.picture = this.registerForm.value.picture ? this.registerForm.value.picture : '';
      this.user.birthDate = this.registerForm.value.birthDate ? this.registerForm.value.birthDate : '';

      this.service.saveUser(user).subscribe(newUser => {
        this.user = newUser;
        alert("You're a birdpeeker now!");
        this.router.navigate(["/user"]);
      })
    } else {
      alert("Oops! Something's wrong! Please confirm your data and try again.")
    }
  }

}
