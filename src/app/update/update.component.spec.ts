import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';

import { UpdateComponent } from './update.component';

describe('UpdateComponent', () => {
  let component: UpdateComponent;
  let fixture: ComponentFixture<UpdateComponent>;

  let birdService;

  const bird: Bird = {id: 1, name: 'Rolinha', image: 'URL'}

  beforeEach(async () => {
    
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ UpdateComponent ]
    })
    .compileComponents();
    birdService = TestBed.inject(BirdService);
    spyOn(birdService, 'getById').and.returnValue(of(bird))
    spyOn(birdService, 'update').and.returnValue(of(bird))

    fixture = TestBed.createComponent(UpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should find a bird by ID', () => {
    component.findBird(1);
    fixture.detectChanges();
    expect(component.bird.id).toEqual(bird.id);
  });

  it('should update a bird', inject([Router], (router: Router) => {
    const event = spyOn(component, 'updateBird');
    component.updateBird(bird);
    expect(event).toHaveBeenCalled();
  }));
});
