import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  bird: Bird = {} as Bird
  
  constructor(
    private service: BirdService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.findBird(id);
  }

  findBird(id: number) {
    this.service.getById(id).subscribe(result => {
      this.bird = result
    })
  }

  updateBird(bird: Bird) {
    this.service.update(bird).subscribe(updated => {
      this.bird = updated
    })
    alert("There you go!");
    this.router.navigate(['/single-bird/', bird.id])
  }

}
