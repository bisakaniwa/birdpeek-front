import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Injectable } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';

import { ResultComponent } from './result.component';

describe('ResultComponent', () => {
  let component: ResultComponent;
  let fixture: ComponentFixture<ResultComponent>;
  const birds: Bird[] = [{name: 'Rolinha', image: 'URL'}];

  class BirdServiceMock extends BirdService{
    
    override getByNamePtEn():Observable<Bird[]>{
      return of(birds);
    }
  }

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ ResultComponent ],
      providers: [{provide: BirdService, useClass: BirdServiceMock}]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should find by name', ()=>{
    component.searchByNamePtEn('Rolinha');
    expect(component.birds).toEqual(birds);
  })
});
