import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Bird } from '../interface/Bird.interface';
import { BirdService } from '../service/bird.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  birds: Bird[] = []

  constructor(
    private service: BirdService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    const search = String(this.activatedRoute.snapshot.paramMap.get('search'));
    this.searchByNamePtEn(search);
  }

  searchByNamePtEn(search: string) {
    this.service.getByNamePtEn(search).subscribe(result => {
      this.birds = result;
      if (this.birds.length === 0) {
        this.notFound()
      }
    })
  }

  notFound() {
    return this.router.navigate(['**'])
  }
}
