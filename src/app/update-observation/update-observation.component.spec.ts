import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { Bird } from '../interface/Bird.interface';
import { Observation } from '../interface/Observation.interface';
import { BirdService } from '../service/bird.service';
import { ObservationService } from '../service/observation.service';

import { UpdateObservationComponent } from './update-observation.component';

describe('UpdateObservationComponent', () => {
  let component: UpdateObservationComponent;
  let fixture: ComponentFixture<UpdateObservationComponent>;
  let observationService;
  let birdService;

  const birds: Bird[] = [{ name: 'Rolinha', image: 'URL' }]
  const observation: Observation = {
    idObs: 1,
    birdObs: 'Rolinha',
    dateObs: '01/11/2022',
    timeObs: '14:00',
    locationObs: 'right here'
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [UpdateObservationComponent]
    })
      .compileComponents();

    observationService = TestBed.inject(ObservationService);
    birdService = TestBed.inject(BirdService);

    spyOn(observationService, 'getById').and.returnValue(of(observation));
    spyOn(observationService, 'getAll').and.returnValue(of([observation]));
    spyOn(observationService, 'update').and.returnValue(of(observation));
    spyOn(birdService, 'getAll').and.returnValue(of(birds));
    
    fixture = TestBed.createComponent(UpdateObservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get a list of all birds', () => {
    component.getAll();
    expect(component.birds).toBe(birds);
  });

  it('should find an observation by ID', () => {
    component.findObservation(1);
    expect(component.observation.idObs).toEqual(observation.idObs);
  });

  it('should update an observation', () => {   
    component.updateObservation(observation);
    expect(component.observation).toBe(observation);
  })
});
