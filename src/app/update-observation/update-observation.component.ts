import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Bird } from '../interface/Bird.interface';
import { Observation } from '../interface/Observation.interface';
import { BirdService } from '../service/bird.service';
import { ObservationService } from '../service/observation.service';

@Component({
  selector: 'app-update-observation',
  templateUrl: './update-observation.component.html',
  styleUrls: ['./update-observation.component.css']
})
export class UpdateObservationComponent implements OnInit {

  observation: Observation = {} as Observation
  bird: Bird = {} as Bird
  birds: Bird[] = []
  
  constructor(
    private service: ObservationService,
    private birdService: BirdService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const idObs = Number(this.activatedRoute.snapshot.paramMap.get('idObs'));
    this.findObservation(idObs);
    this.getAll();
  }

  getAll(): void {
    this.birdService.getAll().subscribe(allBirds => {
      this.birds = allBirds;
    })
  }

  findObservation(idObs: number) {
    this.service.getById(idObs).subscribe(result => {
      this.observation = result 
    })
  }

  updateObservation(observation: Observation) {
    this.service.update(observation).subscribe(updated => {
      this.observation = updated
    })
    alert("There you go!");
    this.router.navigate(['/my-observations']);
  }

}
